import jdk.jfr.Name;
import jdk.jfr.Unsigned;

public class Wizzard extends Character implements ManaUser {

    private int mana;

    public void attack(Character target) {
        mana = -10;
        if(mana >= 10){
            target.takeDamage(30);
            mana-=10;
        }
    }

    public Wizzard(String name, Race race) {
        super(10, 10, 20, name, race);
        mana = 30;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void giveMana(ManaUser target){
        if(health > 10){
            target.restoreMana(5);
            health-=10;
        }
    }

    public void restoreMana(int quantity) {
        if(quantity < 0){
            quantity = 0;
        }
        mana+=quantity;
    }

    public String toString() {
        return super.toString() + String.format(" M> %d", mana);
    }
}
