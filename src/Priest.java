public class Priest extends Character implements ManaUser, InterfaceVide{

    private int mana;

    public void attack(Character target) {
        target.takeDamage(1);
    }

    public void heal(Character target){
        if (mana >= 10){
            target.restoreHealth(5);
            mana -= 10;
        }
    }

    public void heal(){
        heal(this);
    }

    public Priest(String name, Race race) {
        super(20, 0, 15, name, race);
        mana = 20;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void restoreMana(int quantity) {
        if(quantity < 0){
            quantity = 0;
        }
        mana+=quantity;
    }

    public String toString() {
        return super.toString() + String.format(" M> %d", mana);
    }
}
