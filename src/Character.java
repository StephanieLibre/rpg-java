public abstract class Character {
    protected int maxHealth;
    protected int health;
    protected int stamina;
    protected int armor;
    protected String name;
    protected Race race;

    public Character(int stamina, int armor, int maxHealth, String name, Race race) {
        health = maxHealth;
        this.maxHealth = maxHealth;
        this.stamina = stamina;
        this.armor = armor;
        this.name = name;
        this.race = race;
    }

    public abstract void attack(Character target);

    public int getHealth() {
        return health;
    }

    public int getStamina() {
        return stamina;
    }

    public int getArmor() {
        return armor;
    }

    public String getName() {
        return name;
    }

    public int[] getStats() {
        int[] stats = {armor, health, stamina};
        return stats;
    }

    public int takeDamage(int damage){
        damage -= armor;
        if(damage < 0){
            damage = 0;
        }
//        health = health - damage
        health -= damage;
        if (health<0){
            health = 0;
        }
        return health;
    }

    public int restoreHealth(int healthToRestore){
        if(healthToRestore < 0){
            healthToRestore = 0;
        }
        health+=healthToRestore;
        if(health > maxHealth){
            health = maxHealth;
        }
        return health;
    }

    @Override
    public String toString() {
//        return "[" + getClass().getName() +"] "+ getName() +" : H> " + health+"/"+maxHealth + " S>" + stamina;
        return String.format(
                "[%s] %s : H> %d/%d S> %d",
                getClass().getSimpleName(),
                getName(),
                health,
                maxHealth,
                stamina
                );
}
}
