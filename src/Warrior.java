public class Warrior extends Character{

    public void attack(Character target) {
        if(stamina >= 5){
            target.takeDamage(25);
            stamina-=5;
        }
    }

    public Warrior(String name, Race race) {
        super(20, 10, 30, name, race);
        switch (race) {
            case ELF -> armor = 0;
            case ORC -> armor = 30;
        }
    }
}
