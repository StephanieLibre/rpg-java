public class Main {
    public static void main(String[] args) {
        Warrior perso1 = new Warrior("Leeroy", Race.ORC);
        Archer perso2 = new Archer("Legolas", Race.ELF);
        Wizzard perso3 = new Wizzard("Merlin", Race.HUMAN);
        Priest perso4 = new Priest("Dunarr", Race.DWARF);


        System.out.println("======================");
        System.out.println(perso1);
        System.out.println(perso2);

        perso1.attack(perso2);

        System.out.println("======================");
        System.out.println(perso1);
        System.out.println(perso2);

        perso1.attack(perso2);

        System.out.println("======================");
        System.out.println(perso1);
        System.out.println(perso2);

        perso2.attack(perso1);

        System.out.println("======================");
        System.out.println(perso1);
        System.out.println(perso2);
        System.out.println(perso4);


        perso4.heal(perso2);

        System.out.println("======================");
        System.out.println(perso4);
        System.out.println(perso3);
        System.out.println(perso2);

        perso3.giveMana(perso4);

        System.out.println("======================");
        System.out.println(perso3);
        System.out.println(perso4);






    }
}
